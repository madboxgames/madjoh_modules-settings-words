define(function(){
	var Words = {
		// GENERIC
			just_dare : {
				fr : 'JUST DARE',
				en : 'JUST DARE'
			},
			justdare : {
				fr : 'Ose !',
				en : 'Just Dare !'
			},
			loading : {
				fr : 'Chargement...',
				en : 'Loading...'
			},
			loading_personal_data : {
				fr : 'Chargement de vos données personnelles...',
				en : 'Loading your personal data...'
			},
			search : {
				fr : 'Rechercher',
				en : 'Search'
			},
			go : {
				fr : 'GO !',
				en : 'GO !'
			},
			ok : {
				fr : 'OK',
				en : 'OK'
			},
			yes : {
				fr : 'Oui',
				en : 'Yes'
			},
			no : {
				fr : 'Non',
				en : 'No'
			},
			finish : {
				fr : 'TERMINER',
				en : 'FINISH'
			},
			validate : {
				fr : 'VALIDER',
				en : 'VALIDATE'
			},
			create : {
				fr : 'Créer',
				en : 'Create'
			},
			edit : {
				fr : 'Modifier',
				en : 'Edit'
			},
			my_coins : {
				fr : 'MES JETONS',
				en : 'MY COINS'
			},
			find : {
				fr : 'Retrouver',
				en : 'Find'
			},
			no_filtered_friends : {
				fr : 'Aucun utilisateur ne correspond à la recherche.',
				en : 'No user found'
			},
			skip : {
				fr : 'Passer',
				en : 'Skip'
			},
			refreshing : {
				fr : 'Chargement...',
				en : 'Refreshing...'
			},
			retry : {
				fr : 'Réessayer',
				en : 'Retry'
			},

		// SIGN IN & SIGN UP
			email : {
				fr : 'Adresse e-mail',
				en : 'E-mail address'
			},
			password : {
				fr : 'Mot de passe',
				en : 'Password'
			},
			confirm : {
				fr : 'Confirmer',
				en : 'Confirm'
			},
			firstname : {
				fr : 'Prénom',
				en : 'First name'
			},
			lastname : {
				fr : 'Nom',
				en : 'Last name'
			},
			country : {
				fr : 'Pays',
				en : 'Country'
			},
			forgot_password : {
				fr : 'Mot de passe oublié ?',
				en : 'Forgot your password ?'
			},
			connect : {
				fr : 'Connexion',
				en : 'Connect'
			},
			sign_up : {
				fr : 'S\'inscrire',
				en : 'Sign up'
			},
			signing_up : {
				fr : 'Inscription',
				en : 'Sign up'
			},
			connect_facebook : {
				fr : 'Connexion Facebook',
				en : 'Facebook Connect'
			},
			read_agree : {
				fr : 'J\'ai lu et j\'accepte les ',
				en : 'I have read and agreed to the '
			},
			terms_conditions : {
				fr : 'conditions d\'utilisation.',
				en : 'terms and conditions.'
			},

		// TUTORIAL
			tutorial : {
				fr : 'Tutoriel',
				en : 'Tutorial'
			},
			connect_apps : {
				fr : 'Connectez vos applications',
				en : 'Connect your apps'
			},
			exchangeable_coin : {
				fr : 'Jeton utilisable dans toutes nos applications.',
				en : 'Coins transitting between all of our apps'
			},
			create_your_challenges : {
				fr : 'Créez des défis',
				en : 'Create challenges'
			},
			challenge_your_friends : {
				fr : 'Envoyez des défis',
				en : 'Send challenges'
			},
			receive_challenges : {
				fr : 'Recevez des défis',
				en : 'Receive challenges'
			},
			validate_challenges : {
				fr : 'Validez des défis',
				en : 'Validate challenges'
			},
			comment_like_resend : {
				fr : 'Commentez, aimez et relancez les défis !',
				en : 'Comment, like and resend challenges !'
			},
			add_friends : {
				fr : 'Ajoutez des amis',
				en : 'Add friends'
			},
			follow_stars : {
				fr : 'Suivez des stars',
				en : 'Follow stars'
			},
			challenge_world : {
				fr : 'Defiez le monde entier',
				en : 'Challenge the entire world'
			},
			challenge_example : {
				fr : 'Je te défie de danser la samba !',
				en : 'Just dare dancing on samba !'
			},
			challenge_example_author : {
				fr : 'Par Pierredu77',
				en : 'By Peter77'
			},
			challenges_worldwide : {
				fr : 'Creez des defis qui feront le tour du monde !',
				en : 'Create challenges that will go virale !'
			},
			add_friends_from : {
				fr : 'AJOUTE TES AMIS A PARTIR DE',
				en : 'ADD YOUR FRIENDS FROM'
			},
			friends_already_here : {
				fr : 'Importez vos amis depuis Facebook',
				en : 'Import you friends from Facebook'
			},
			friends_import : {
				fr : 'Invitez vos amis à rejoindre JustDare',
				en : 'Invite your friends to join JustDare'
			},
			import_friends : {
				fr : 'Importer',
				en : 'Import'
			},
			invite_them : {
				fr : 'Inviter',
				en : 'Invite'
			},

		// CHALLENGES 
			challenges : {
				fr : 'Defis',
				en : 'Dares'
			},
			sending : {
				fr : 'Envoi',
				en : 'Sending'
			},
			sent : {
				fr : 'Envoyés',
				en : 'Sent'
			},
			reception : {
				fr : 'Réception',
				en : 'Reception'
			},
			validation : {
				fr : 'Validation',
				en : 'Validation'
			},
			create_new_challenge : {
				fr : '+ CREER UN NOUVEAU DEFI',
				en : '+ CREATE NEW CHALLENGE'
			},
			make_viral : {
				fr : 'CREEZ LE BUZZ !',
				en : 'MAKE IT VIRALE !'
			},
			challenge_format : {
				fr : 'Je te défie de ...',
				en : 'Just dare ...'
			},
			delay : {
				fr : 'Durée : ',
				en : 'Delay : '
			},
			no_challengeable_friends : {
				fr : 'Vous n\'avez aucun ami à défier...<br/>Allez ajouter des amis !',
				en : 'You have no friend to challenge....<br/>Go add some friends !'
			},
			send : {
				fr : 'Envoyer',
				en : 'Send'
			},
			no_sent_challenges : {
				fr : 'Vous n\'avez envoyé aucun défi pour l\'instant.<br/>Montrez à vos amis que vous êtes actif, envoyez-en !',
				en : 'You haven\'t sent any challenge yet.<br/>Show your friends that you are active, go send some !'
			},
			no_received_challenges : {
				fr : 'Vous n\'avez aucun défi en attente de réalisation.<br/>Montrez à vos amis que vous êtes actif, envoyez-en !',
				en : 'You don\'t have any challenge to execute.<br/>Show your friends that you are active, go send some !'
			},
			expired : {
				fr : 'Expiré',
				en : 'Expired'
			},
			challenger : {
				fr : 'Challenger',
				en : 'Challenger'
			},
			group : {
				fr : 'Groupe',
				en : 'Group'
			},
			'public'  : {
				fr : 'Public',
				en : 'Public'
			},
			accept_challenge : {
				fr : 'Cap !',
				en : 'I dare !'
			},
			done_title : {
				fr : 'Ça c\'est fait !',
				en : 'That\'s done !' 
			},
			invalidate : {
				fr : 'Invalider',
				en : 'Invalidate'
			},
			validate : {
				fr : 'Valider',
				en : 'Validate'
			},
			add_proof : {
				fr : 'Ajoutez une preuve',
				en : 'Add a proof'
			},
			nudge : {
				fr : 'Relancer',
				en : 'Nudge'
			},

		// ACTUALITIES
			actualities : {
				fr : 'Actualites',
				en : 'Feed'
			},
			best_of : {
				fr : 'BEST OF',
				en : 'BEST OF'
			},
			top_dares : {
				fr : 'TOP DARES',
				en : 'TOP DARES'
			},
			x_users_like_this : {
				fr : '<span class="nb-likes">{{nb}} personnes</span> aiment ça.',
				en : '<span class="nb-likes">{{nb}} people</span> like this.'
			},
			one_user_likes_this : {
				fr : '<span class="nb-likes">1 personne</span> aime ça.',
				en : '<span class="nb-likes">1 person</span> likes this.'
			},
			they_liked_it : {
				fr : 'Ils ont aimé ce défi',
				en : 'They liked this challenge'
			},
			comment : {
				fr : 'Commenter',
				en : 'Comment'
			},
			no_recent_challenges : {
				fr : 'JustDare c\'est beaucoup mieux avec ses amis et ses stars préférées !<br/>Rendez vous vite sur la page Amis pour y remédier !',
				en : 'JustDare is way better with your friends and favorite stars !<br/>Hurry to the Friends page to fix your situation !'
			},
			no_best_one_challenges : {
				fr : 'Ce défi n\'a jamais été réalisé ou bien aucune de ses réalisations ne vous est accessible.',
				en : 'This challenge has never been executed or none of its realizations are accessible to you.'
			},
			no_filtered_challenges : {
				fr : 'Aucun défi ne correspond à votre recherche !',
				en : 'No match found'
			},
			join_us : {
				fr : 'Rejoins-nous sur JustDare !',
				en : 'Join us on JustDare !'
			},
			just_now : {
				fr : 'à l\'instant',
				en : 'just now'
			},
			hide : {
				fr : 'Masquer',
				en : 'Hide'
			},
			'delete' : {
				fr : 'Supprimer',
				en : 'Delete'
			},
			signal : {
				fr : 'Signaler',
				en : 'Signal'
			},
			by_user : {
				fr : 'par {{user}}',
				en : 'by {{user}}'
			},
			to_user : {
				fr : '{{challenge}} à {{user}}',
				en : '{{challenge}} to {{user}}'
			},
			user_no_time : {
				fr : '<span class="user-friend">{{user}}</span> n\'a pas eu le temps de {{challenge}}',
				en : '<span class="user-friend">{{user}}</span> didn\'t get the time to dare {{challenge}}'
			},
			user_refused : {
				fr : '<span class="user-friend">{{user}}</span> n\'a pas osé {{challenge}}',
				en : '<span class="user-friend">{{user}}</span> didn\'t dare {{challenge}}'
			},

		// FRIENDS
			friendship : {
				fr : 'Amitiés',
				en : 'Friendships'
			},
			follow : {
				fr : 'Suivre',
				en : 'Follow'
			},
			invite : {
				fr : 'Ajouter',
				en : 'Add Friend'
			},
			following : {
				fr : 'Suivi',
				en : 'Following'
			},
			unfollow : {
				fr : 'Se désinscrire',
				en : 'Unfollow'
			},
			decline : {
				fr : 'Refuser',
				en : 'Decline'
			},
			accept : {
				fr : 'Accepter',
				en : 'Accept'
			},
			waiting : {
				fr : 'En attente',
				en : 'Waiting'
			},
			add : {
				fr : 'Ajouts',
				en : 'Add'
			},
			friend : {
				fr : 'Ami',
				en : 'Friend'
			},
			friends : {
				fr : 'Amis',
				en : 'Friends'
			},
			pending : {
				fr : 'En attente',
				en : 'Pending'
			},
			suggestions : {
				fr : 'Suggestions',
				en : 'Suggestions'
			},
			no_suggested_friends : {
				fr : 'Aucun utilisateur ne peut vous être suggéré.',
				en : 'No match found'
			},
			find_friends_networks : {
				fr : 'Importez vos amis',
				en : 'Bring your friends'
			},
			select_all : {
				fr : 'Ajouter tout le monde',
				en : 'Select all'
			},
			x_sent : {
				fr : '{{number}} défis envoyés.',
				en : '{{number}} challenges sent.'
			},
			x_realized : {
				fr : '{{number}} défis réalisés.',
				en : '{{number}} challenges executed.'
			},
			x_created : {
				fr : '{{number}} défis créés.',
				en : '{{number}} challenges created.'
			},

			x_friends : {
				fr : '{{number}} Amis',
				en : '{{number}} Friends'
			},
			x_followers : {
				fr : '{{number}} Followers',
				en : '{{number}} Followers'
			},
			x_common_friends : {
				fr : '{{number}} amis en commun',
				en : '{{number}} common friends'
			},

		// MENU
			menu : {
				fr : 'MENU',
				en : 'MENU'
			},
			social : {
				fr : 'Social',
				en : 'Social'
			},
			my_friends : {
				fr : 'Mes Amis',
				en : 'My Friends'
			},
			my_followers : {
				fr : 'Mes Followers',
				en : 'My Followers'
			},
			my_idoles : {
				fr : 'Mes Idoles',
				en : 'My Idoles'
			},
			followers : {
				fr : 'Followers',
				en : 'Followers'
			},
			idoles : {
				fr : 'Idoles',
				en : 'Idoles'
			},
			no_pending : {
				fr : 'Vous n\'avez aucune invitation en attente.',
				en : 'You have no pending invitation.'
			},
			no_friends : {
				fr : 'Vous n\'avez pas encore d\'amis.',
				en : 'You don\'t have any friends yet.'
			},
			no_followers : {
				fr : 'Personne ne vous suit pour l\'instant',
				en : 'Nobody\'s following you for now.'
			},
			no_idoles : {
				fr : 'Vous ne suivez personne pour l\'instant.',
				en : 'You\‘re not following anyone for now.'
			},
			go_add_friends : {
				fr : 'Accéder à l\'ajout d\'amis',
				en : 'Go add friends'
			},
			go_add_facebook : {
				fr : 'Ajouter depuis Facebook',
				en : 'Go add from Facebook'
			},
			profile : {
				fr : 'Profil',
				en : 'Profile'
			},
			settings : {
				fr : 'Paramètres',
				en : 'Settings'
			},
			legal_terms : {
				fr : 'Conditions d\'utilisation',
				en : 'Legal Terms'
			},
			about_madjoh : {
				fr : 'À propos de MadJoh',
				en : 'About MadJoh'
			},
			visit_website : {
				fr : 'Visitez notre site !',
				en : 'Visit our website !'
			},
			rank_application : {
				fr : 'Notez l\'application',
				en : 'Rank this application'
			},
			follow_madjoh : {
				fr : 'Suivez MadJoh sur les réseaux sociaux',
				en : 'Follow MadJoh on the social networks'
			},
			logout : {
				fr : 'Déconnexion',
				en : 'Logout'
			},
			switch_env : {
				fr : 'Switch Environment',
				en : 'Switch Environment'
			},
	
		// PROFILE
			my_account : {
				fr : 'Mon Compte',
				en : 'My Account'
			},
			birthdate : {
				fr : 'Anniversaire',
				en : 'Birthdate'
			},
			location : {
				fr : 'Localisation',
				en : 'Location'
			},
			change_password_title : {
				fr : 'Modifiez votre mot de passe',
				en : 'Change your password'
			},
			old_password : {
				fr : 'Ancien mot de passe',
				en : 'Old password'
			},
			new_password : {
				fr : 'Nouveau mot de passe',
				en : 'New password'
			},
			newsletter : {
				fr : 'Newsletter',
				en : 'Newsletter'
			},
			delete_account_text : {
				fr : 'Désactiver mon compte',
				en : 'Desactivate my account'
			},
			update : {
				fr : 'Mettre à jour',
				en : 'Update'
			},
	
		// NOTIFICATIONS 
			notifications : {
				fr : 'notifications',
				en : 'notifications'
			},
			no_notifications : {
				fr : 'Vous n\'avez aucun notification.',
				en : 'You have no notification.'
			},

			user_and_x_other : {
				fr : '{{user}} et <span class="other-friends">{{number}} autre(s) personne(s)</span>',
				en : '{{user}} and <span class="other-friends">{{number}} other</span>',
			},

			user_sent_you_challenge : {
				code : 0,
				fr : '{{user}} vous a envoyé un <span class="challenge">défi</span>',
				en : '{{user}} sent you a <span class="challenge">challenge</span>'
			},
			user_executed_your_challenge : {
				code : 1,
				fr : '{{user}} a réalisé votre <span class="challenge">défi</span>',
				en : '{{user}} executed your <span class="challenge">challenge</span>'
			},
			user_validated_your_challenge : {
				code : 2, // DEPRECATED
				fr : '{{user}} a validé votre <span class="challenge">défi</span>',
				en : '{{user}} validated your <span class="challenge">challenge</span>'
			},
			user_invalidated_your_challenge : {
				code : 3, // DEPRECATED
				fr : '{{user}} a refusé de valider votre <span class="challenge">défi</span>',
				en : '{{user}} refused to validate your <span class="challenge">challenge</span>'
			},
			user_refused_execute_challenge : {
				code : 4,
				fr : '{{user}} a refusé d\'exécuter votre <span class="challenge">défi</span>',
				en : '{{user}} refused to execute your <span class="challenge">challenge</span>'
			},
			user_missed_your_challenge : {
				code : 5,
				fr : '{{user}} n\'a pas eu le temps d\'exécuter votre <span class="challenge">défi</span>',
				en : '{{user}} didn\'t have time to execute your <span class="challenge">challenge</span>'
			},
			user_nudged_you : {
				code : 6,
				fr : '{{user}} vous a relancé sur un <span class="challenge">défi</span>',
				en : '{{user}} nudged you on a <span class="challenge">challenge</span>'
			},
			user_challenge_missed : {
				code : 7,
				fr : '{{user}} vous a envoyé un <span class="challenge">défi</span> que vous avez manqué...',
				en : '{{user}} sent you a <span class="challenge">challenge</span> that you missed...'
			},
			user_liked_your_challenge : {
				code : 10,
				fr : '{{user}} a aimé votre <span class="challenge">défi</span>',
				en : '{{user}} liked your <span class="challenge">challenge</span>'
			},
			user_commented_your_challenge : {
				code : 11,
				fr : '{{user}} a commenté votre <span class="challenge">défi</span>',
				en : '{{user}} commented on your <span class="challenge">challenge</span>'
			},
			user_resent_your_challenge : {
				code : 12, // DEPRECATED
				fr : '{{user}} a relancé votre <span class="challenge">défi</span>',
				en : '{{user}} resent your <span class="challenge">challenge</span>'
			},
			user_shared_your_challenge : {
				code : 13,
				fr : '{{user}} a partagé votre <span class="challenge">défi</span>',
				en : '{{user}} shared your <span class="challenge">challenge</span>'
			},
			user_liked_challenge_you_sent : {
				code : 20,
				fr : '{{user}} a aimé un <span class="challenge">défi</span> que vous avez envoyé',
				en : '{{user}} liked a <span class="challenge">challenge</span> you sent'
			},
			user_commented_challenge_you_sent : {
				code : 21,
				fr : '{{user}} a commenté un <span class="challenge">défi</span> que vous avez envoyé',
				en : '{{user}} commented on a <span class="challenge">challenge</span> you sent'
			},
			user_resent_challenge_you_sent : {
				code : 22, // DEPRECATED
				fr : '{{user}} a relancé un <span class="challenge">défi</span> que vous avez envoyé',
				en : '{{user}} resent a <span class="challenge">challenge</span> you sent'
			},
			user_shared_challenge_you_sent : {
				code : 23,
				fr : '{{user}} a partagé un <span class="challenge">défi</span> que vous avez envoyé',
				en : '{{user}} shared a <span class="challenge">challenge</span> you sent'
			},
			user_commented_challenge_you_commented : {
				code : 31,
				fr : '{{user}} a commenté sur un <span class="challenge">défi</span> que vous avez aussi commenté',
				en : '{{user}} commented on a <span class="challenge">challenge</span> you also commented'
			},
			user_commented_challenge_you_liked : {
				code : 41,
				fr : '{{user}} a commenté sur un <span class="challenge">défi</span> que vous avez aimé',
				en : '{{user}} commented on a <span class="challenge">challenge</span> you liked'
			},
			user_frienship_invited : {
				code : 100,
				fr : '{{user}} vous demande en <span class="friendship">ami</span>',
				en : '{{user}} sent you a <span class="friendship">friendship invitation</span>'
			},
			user_follows_you : {
				code : 101,
				fr : '{{user}} vous suit',
				en : '{{user}} follows you'
			},
			user_is_friend : {
				code : 102,
				fr : '{{user}} est désormais votre ami',
				en : '{{user}} is now your friend'
			},
			user_requesting_x_coins : {
				code : 110,
				fr : '{{user}} vous demande {{number}} pièces',
				en : '{{user}} is asking for {{number}} coins'
			},
			user_sent_x_coins : {
				code : 111,
				fr : '{{user}} vous a envoyé {{number}} pièces',
				en : '{{user}} sent you {{number}} coins'
			},
			user_sent_x_coins : {
				code : 120,
				fr : '{{user}} vous a envoyé {{number}} pièces',
				en : '{{user}} sent you {{number}} coins'
			},

		// GROUPS
			your_groups : {
				fr : 'Vos groupes',
				en : 'Your groups'
			},
			group_details : {
				fr : 'Détails du groupe',
				en : 'Group details'
			},
			new_group : {
				fr : 'Mon Groupe',
				en : 'New Group'
			},
			group_name : {
				fr : 'NOM DU GROUPE',
				en : 'GROUP NAME'
			},
			no_new_group_friends : {
				fr : 'Vous n\'avez pas d\'amis à ajouter au groupe.',
				en : 'You have no friends to add to a group.'
			},
			create_group : {
				fr : 'CREER UN GROUPE',
				en : 'CREATE A GROUP'
			},

		// EARN MONEY
			more_coins : {
				fr : 'Plus de jetons',
				en : 'More coins'
			},
			watch_ad : {
				fr : 'Voir une pub',
				en : 'Watch ad'
			},
			invite_friend : {
				fr : 'Inviter un ami',
				en : 'Invite a friend'
			},
			fill_profile : {
				fr : 'Renseigner son profil',
				en : 'Complete profile'
			},
			please_connect : {
				fr : 'Connectez vous',
				en : 'Connect'
			},
			visit_page : {
				fr : 'Visitez notre page',
				en : 'Visit our page'
			},
			ask_friends_coins : {
				fr : 'Demander des jetons à vos amis',
				en : 'Ask your friends for coins'
			},
			give_friends_coins : {
				fr : 'Envoyer des jetons à vos amis',
				en : 'Send coins to your friends'
			},
			request : {
				fr : 'Demandes',
				en : 'Requesting'
			},
	
		// TIME COMPARATOR
			time_initials_days_hours : {
				fr : '{{day}}j {{hour}}h',
				en : '{{day}}d {{hour}}h'
			},
			time_initials_hours_minutes : {
				fr : '{{hour}}h {{min}}m',
				en : '{{hour}}h {{min}}m'
			},
			time_initials_minutes_seconds : {
				fr : '{{min}}m {{sec}}s',
				en : '{{min}}m {{sec}}s'
			},

			time_left : {
				fr : '{{time}} restants',
				en : '{{time}} left'
			},

		// MESSAGES
			// TITLES
				oops : {
					fr : 'Oups...',
					en : 'Oops...'
				},
				welcome : {
					fr : 'BIENVENUE',
					en : 'WELCOME'
				},
				warning : {
					fr : 'Attention',
					en : 'Warning'
				},
				fiou : {
					fr : 'Ouf !',
					en : 'Fiou !'
				},
				done : {
					fr : 'Fait !',
					en : 'Done !'
				},
				bye : {
					fr : 'BYE !',
					en : 'BYE !'
				},
				yay : {
					fr : 'BRAVO !',
					en : 'YAY !'
				},
				thanks : {
					fr : 'MERCI !',
					en : 'THANKS !'
				},

			// GENERIC
				message_error : {
					fr : 'Une erreur a eu lieu.<br/>Réessayez plus tard !',
					en : 'An error occured.<br/>Try again later !'
				},
				message_server_error : {
					fr : 'Une erreur a eu lieu sur le serveur.<br/>Réessayez plus tard !',
					en : 'An error occured on the server.<br/>Please try again later !'
				},
				message_internet_error : {
					fr : 'Une erreur a eu lieu.<br/>Vérifiez votre connexion internet et réessayez !',
					en : 'An error occured.<br/>Check out your internet connection and try again !'
				},
				message_deprecated_version : {
					fr : 'Vous possédez une version trop ancienne de l\'application.<br/>Veuillez la mettre à jour pour continuer.',
					en : 'Your app version is too old.<br/>Please upgrade to the latest one.'
				},
				message_retry : {
					fr : 'Une erreur a eu lieu.<br/>Cliquez pour réessayer',
					en : 'An error occured.<br/>Click to retry'
				},
				message_unavailable : {
					fr : 'Cette fonctionnalité n\'est pas disponible !<br/>Elle le sera sûrement plus tard, soyez patients.',
					en : 'This feature is unavailable !<br/>It will probably be working later, be patient.'
				},
				message_wrong_file_format : {
					fr : 'Ce format de fichier n\'est pas supporté !<br/>Les formats supportés sont :<br/>Photo : jpg, jpeg, gif ou png<br/>Vidéo : mp4 ou mov<br/>Son : mp3 ou wav',
					en : 'This file extension is not supported !<br/>Try one of these :<br/>Pictures : jpg, jpeg, gif or png<br/>Video : mp4 or mov<br/>Audio : mp3 or wav'
				},
				message_wrong_picture_format : {
					fr : 'Ce format de photo n\'est pas supporté !<br/>Les formats supportés sont :<br/>jpg, jpeg ou gif ou png',
					en : 'This picture extension is not supported !<br/>Try one of these :<br/>jpg, jpeg, gif or png'
				},
				message_need_facebook : {
					fr : 'Vous devez être connecté avec Facbeook pour profiter de cette fonctionnalité.',
					en : 'You need to be logged in with facebook to use this feature.'
				},

			// TUTORIAL
				message_welcome : {
					fr : 'Allez dès à présent ajouter vos amis et créez vos premiers défis !',
					en : 'Go add your friends and create your first challenge !'
				},

			// PREFERENCES
				message_sync_error : {
					fr : 'Vous avez été déconnecté...',
					en : 'You got disconnected...'
				},
				message_upload_failed : {
					fr : 'Votre progression n\'a pas pu être enregistrée.<br/>Vérifiez votre connexion internet.',
					en : 'Your progression couldn\'t be saved.<br/>Check your internet connection.'
				},
				message_upload_recovering : {
					fr : 'Vous avez retrouvé votre connexion internet.<br/>Nous en profitons pour enregistrer votre progression.',
					en : 'It seems like you found an internet connection.<br/>Let\'s upload your progression.'
				},

			// ACCOUNT
				message_wrong_confirmation : {
					fr : 'Vous avez mal confirmé votre mot de passe.<br/>Réessayez !',
					en : 'You misspelled the confirmation of your password.<br/>Try again !'
				},
				message_accept_optine : {
					fr : 'Veuillez accepter les conditions d\'utilisation pour vous inscrire.',
					en : 'Please accept the terms and confitions before you sign up'
				},
				message_forgot_password : {
					fr : 'Un mail vous a été envoyé avec votre nouveau mot de passe.<br/>Utilisez le pour vous connecter.<br/>Vous pouvez le modifier dans votre page de profil',
					en : 'You\'ve been sent an email with your new password.<br/>Use it to login.<br/>You can set a new one in your profile page.'
				},
				message_goodbye : {
					fr : 'Votre profile a été désactivé avec succès.<br/>Vous allez être déconnecté.<br/>Toute reconnexion réactivera votre profile.',
					en : 'Your account has been desactivated.<br/>You will be disconnected.<br/>Your account will be reactivated if you log back in.'
				},
				message_inputs_error : {
					fr : 'Les champs renseignés ne sont pas valides.',
					en : 'The specified inputs are not valid.'
				},
				message_too_young : {
					fr : 'Vous n\'avez pas l\'âge pour utiliser notre application... Revenez dans quelques années !',
					en : 'You are too young to use this application... Come back in a few years !'
				},
				message_remove : {
					fr : 'En désactivant votre compte vous ne pourrez plus naviguer dans l\'application et toutes vos anciennes publications seront invisibles.',
					en : 'If you desactivate your account, you will no longer be able to navigate through the application and all your previous publications will be invisible.'
				},

			// CHALLENGES
				message_invalid_description : {
					fr : 'Votre défi doit faire plus de 20 caractères et respecter la formulation suivante : <br/>"Je te défie de ..."',
					en : 'Your challenge must be at least 15 characters long and must comply with the following formulation :<br/>"Just dare ..."'
				},
				message_first_sent : {
					fr : 'Vous avez envoyé votre premier défi !!<br/><br/>Ajoutez encore plus d\'amis, partagez vos défis et exécutez ceux de vos amis !',
					en : 'You just sent your first challenge !!<br/><br/>Add more friends, share your challenges and dare doing anything !'
				},
				message_file_size : {
					fr : 'Ce fichier fait plus de 10 Mo...<br/>Essayez avec un fichier plus léger.',
					en : 'This file is heavier than 10 Mo...<br/>Try again with a lighter file.'
				},
				message_accept_failure : {
					fr : 'Une erreur interne vous empêche de répondre à ce défi. Réessayez plus tard.',
					en : 'An error occured ! Try answering this challenge later.'
				},
				message_congrats_dare : {
					fr : 'Vous avez réalisé votre premier défi !<br/>En réalisant un défi vous gagnez 5 pièces. Désormais ce défi vous appartient, relancez-le !',
					en : 'You just executed your first challenge !<br/>By doing so you earned 5 coins and this challenge is now yours.<br/> Send it to your friends !'
				},
				message_missing_file : {
					fr : 'Vous devez d\'abord sélectionner une image.',
					en : 'You need to select an image first.'
				},

			// ACTUALITIES
				message_signal : {
					fr : 'Votre signalement a été pris en compte !',
					en : 'Your signalement has been taken into account !'
				},

			// FRIENDS
				message_friend_unfollow : {
					fr : 'Voulez-vous vraiment arrêter de suivre cet utilisateur ?',
					en : 'Do you really wish to stop following this user ?'
				},
				message_friend_invite_cancel : {
					fr : 'Voulez-vous vraiment annuler votre demande d\'amitié ?',
					en : 'Do you really wish to cancel your friendship invitation ?'
				},
				message_friend_remove : {
					fr : 'Vous êtes sur le point de supprimer un ami.<br/>Toutefois il pourra toujours consulter votre profil.<br/> Pour y remédier vous pouvez le bloquer dans votre liste de followers.',
					en : 'You are about to delete a friend.<br/> Although he would still be able to see your posts<br/>To prevent that you can block him on your followers page.'
				},
				message_access_denied : {
					fr : 'Accès refusé !',
					en : 'Access denied !'
				},

			// MONEY
				message_thanks_watching_ad : {
					fr : 'Merci d\'avoir visionné cette vidéo !<br/>Vous gagnez 5 pièces.<br/>Une prochaine vidéo sera disponible dans 2h',
					en : 'Thanks for watching this ad !<br/>You earn 5 coins<br/>Another video will be available in 2 hours'
				},
				message_thanks_visit_facebook : {
					fr : 'Merci d\'avoir visité notre page Facebook !<br/>Vous gagnez 5 pièces.<br/>N\'hésitez pas à nous laisser des commentaires et des likes !',
					en : 'Thank you for visiting our Facebook page !<br/>You earn 5 coins.<br/>Leave us some comments and likes !'
				},
				message_thanks_visit_twitter : {
					fr : 'Merci d\'avoir visité notre page Twitter !<br/>Nous espérons que vous y avez trouvé des choses intéressantes.<br/>Vous gagnez 5 pièces.',
					en : 'Thank you for visiting our Twitter page !<br/>We hope you found some interesting stuff there.<br/>You earn 5 coins.'
				},
				message_thanks_visit_website : {
					fr : 'Merci d\'avoir visité notre site web !<br/>Nous espérons que vous y avez trouvé des choses intéressantes.<br/>Vous gagnez 5 pièces.',
					en : 'Thank you for visiting our website !<br/>We hope you found some interesting stuff there.<br/>You earn 5 coins.'
				},
				message_thanks_profile_editing : {
					fr : 'Merci d\'avoir renseigné votre profil !<br/>Voici 5 pièces pour vous récompenser !',
					en : 'Thank you for filling up your profile informations !<br/>Here are 5 coins as a reward !'
				},
				message_thanks_facebook_connect : {
					fr : 'Merci de vous être connecté avec Facebook !<br/>Voici 5 pièces pour vous récompenser !',
					en : 'Thank you for logging in with Facebook !<br/>Here are 5 coins as a reward !'
				}
	};

	return Words;
});