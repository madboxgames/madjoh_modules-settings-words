define(function(){
	var Messages = {
		// GENERIC
			error : {
				title 	: 'oops',
				message : 'message_error'
			},
			server_error : {
				title 	: 'oops',
				message : 'message_server_error'
			},
			internet_error : {
				title 	: 'oops',
				message : 'message_internet_error',
				ok 		: 'retry'
			},
			deprecated_version : {
				title 	: 'oops',
				message : 'message_deprecated_version'
			},
			retry : {
				title 	: 'oops',
				message : 'message_retry',
				ok 		: 'retry'
			},
			unavailable : {
				title 	: 'oops',
				message : 'message_unavailable'
			},
			wrong_file_format : {
				title 	: 'oops',
				message : 'message_wrong_file_format'
			},
			wrong_picture_format : {
				title 	: 'oops',
				message : 'message_wrong_picture_format'
			},
			need_facebook : {
				title 	: 'oops',
				message : 'message_need_facebook',
				ok 		: 'connect'
			},

		// TUTORIAL
			welcome : {
				title 	: 'welcome',
				message : 'message_welcome'
			},

		// PREFERENCES
			sync_error : {
				title 	: 'oops',
				message : 'message_sync_error'
			},
			upload_failed : {
				title 	: 'warning',
				message : 'message_upload_failed',
				ok 		: 'retry'
			},
			upload_recovering : {
				title 	: 'fiou',
				message : 'message_upload_recovering' 
			},

		// ACCOUNT
			wrong_confirmation : {
				title 	: 'oops',
				message : 'message_wrong_confirmation'
			},
			accept_optine : {
				title 	: 'oops',
				message : 'message_accept_optine'
			},
			forgot_password : {
				title 	: 'done',
				message : 'message_forgot_password'
			},
			goodbye : {
				title 	: 'bye', 
				message : 'message_goodbye'
			},
			inputs_error : {
				title 	: 'oops',
				message : 'message_inputs_error'
			},
			too_young : {
				title 	: 'oops',
				message : 'message_too_young'
			},
			remove : {
				title 	: 'warning',
				message : 'message_remove',
				ok 		: 'confirm'
			},

		// CHALLENGES
			invalid_description : {
				title 	: 'oops',
				message : 'message_invalid_description'
			},
			first_sent : {
				title 	: 'yay',
				message : 'message_first_sent'
			},
			file_size : {
				title 	: 'oops',
				message : 'message_file_size'
			},
			accept_failure : {
				title 	: 'oops',
				message : 'message_accept_failure'
			},
			congrats_dare : {
				title 	: 'yay',
				message : 'message_congrats_dare'
			},
			missing_file : {
				title 	: 'oops',
				message : 'message_missing_file'
			},
		
		// ACTUALITIES
			signal : {
				title 	: 'thanks',
				message : 'message_signal'
			},
	
		// FRIENDS
			friend_unfollow : {
				title 	: 'warning',
				message : 'message_friend_unfollow',
				ok 		: 'confirm'
			},
			friend_invite_cancel : {
				title 	: 'warning',
				message : 'message_friend_invite_cancel',
				ok 		: 'confirm'
			},
			friend_remove : {
				title 	: 'warning',
				message : 'message_friend_remove',
				ok 		: 'confirm'
			},
			access_denied : {
				title 	: 'oops',
				message : 'access_denied'
			},
	
		// MONEY
			thanks_watching_ad : {
				title 	: 'thanks',
				message : 'message_thanks_watching_ad'
			},
			thanks_visit_facebook : {
				title 	: 'thanks',
				message : 'message_thanks_visit_facebook'
			},
			thanks_visit_twitter : {
				title 	: 'thanks',
				message : 'message_thanks_visit_twitter'
			},
			thanks_visit_website : {
				title 	: 'thanks',
				message : 'message_thanks_visit_website'
			},
			thanks_profile_editing : {
				title 	: 'thanks',
				message : 'message_thanks_profile_editing'
			},
			thanks_facebook_connect : {
				title 	: 'thanks',
				message : 'thanks_facebook_connect'
			}
	};

	return Messages;
});